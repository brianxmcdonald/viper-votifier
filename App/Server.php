<?php

namespace App;

use BFoxwell\Votifier\Votifier;
use Mcprohosting\MulticraftApi\MulticraftApi;

class Server
{
    protected $config;
    protected $serverConfig;

    public function __construct(array $config, array $serverConfig)
    {
        $this->config = $config;
        $this->serverConfig = $serverConfig;
    }

    public function run()
    {
        global $argv;

        // Get the server provided via args
        if (!isset($argv[1])) {
            throw new \Exception("Please provide a server to load.");
        }

        // Set the Server Key
        $serverKey = $argv[1];

        // Validate Server Key Exists in the Config
        if (!array_key_exists($serverKey, $this->serverConfig)) {
            throw new \Exception("Invalid server key provided.");
        }

        // Load the Server Config
        $votifierConfig = $this->serverConfig[$serverKey];
        $serverId = $votifierConfig['server_id'];
        $serverLevels = $votifierConfig['levels'];

        // Boot the Multicraft Api
        $mcapi = new MulticraftApi(
            $this->config['url'],
            $this->config['user'],
            $this->config['key']
        );

        $itemsPath = __DIR__ . '/../config-items.php';;
        $itemsAdditionalPath = __DIR__ . '/../config-items-additional.php';

        $this->server = new Votifier($votifierConfig,
            function ($message, $logger) use ($mcapi, $serverId, $serverLevels, $itemsPath, $itemsAdditionalPath) {
                //Load the Items array and Merge Additional Items that are manually added
                $items = include $itemsPath;
                if (file_exists($itemsAdditionalPath)) {
                    $itemsAdditional = include $itemsAdditionalPath;
                    $items = array_merge($items, $itemsAdditional);
                }

                // Load the Vote Object
                $vote = new Vote($items);
                $cmds = $vote->give($message['username'], $serverLevels);

                // Run Each Command
                foreach ($cmds as $cmd) {
                    $mcapi->sendConsoleCommand($serverId, $cmd);
                    //$mcapi->sendAllConsoleCommand($cmd);

                    $date = date("Y-m-d H:i:s");
                    echo "[$date] Ran command (Server ID - $serverId):\r\n $cmd", PHP_EOL;
                }
            });

        $this->server->run();
    }
}