<?php
namespace App;

class Vote
{
	protected $items;

	public function __construct(array $items)
	{
		$this->items = $items;
	}

	public function give($username, $levels = 10)
	{
		$item = $this->random($this->items);
		
		// Replace any instances of %n with the playername
		$item = preg_replace("/\%n/i", $username, $item);

		return [
			"give $username $item",
			"xp $levels"."L $username",
			"tell $username Thank You For Voting!",
			"tell $username You were given $item and $levels levels!"
		];
	}

	protected function random(array $data)
	{
		for($i = 0; $i < mt_rand(10, 1250); $i++)
		{
			shuffle($data);
		}

		return array_pop($data);
	}
}
