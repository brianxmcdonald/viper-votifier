# Vipers Lair Votifier Server

This application automatically gives voters items.

## Setup

 - Copy the `config.php.example` file to `config.php` and modify as needed.
 - Generate public/private key pair using `openssl genrsa -out storage/private.key 2048` and `openssl rsa -in storage/private.key -pubout > storage/public.key`