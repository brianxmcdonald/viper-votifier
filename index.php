<?php
require __DIR__ . '/vendor/autoload.php';
$config = include __DIR__ . '/config.php';
$serverConfig = include __DIR__ . '/config-servers.php';
$server = new App\Server($config, $serverConfig);
$server->run();