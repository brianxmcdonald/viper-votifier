<?php
require __DIR__ . '/vendor/autoload.php';
$config = include __DIR__ . '/config.php';
$items = include __DIR__ . '/config-items.php';

$mcapi = new Mcprohosting\MulticraftApi\MulticraftApi(
	$config['url'],
	$config['user'],
	$config['key']
);

foreach($items as $item)
{
	$username = $config['tester'];
	$mcapi->sendAllConsoleCommand("give $username $item");
}