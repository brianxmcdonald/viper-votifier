<?php

return [
    'vanilla' => [
        'server_id' => 27,
        'key' => __DIR__ . '/storage/vanilla/private.key',
        'port' => 8192,
        'levels' => 10
    ],
    'snapshot' => [
        'server_id' => 4,
        'key' => __DIR__ . '/storage/snapshot/private.key',
        'port' => 8193,
        'levels' => 10
    ],
    'hardcore' => [
        'server_id' => 21,
        'key' => __DIR__ . '/storage/hardcore/private.key',
        'port' => 8194,
        'levels' => 10
    ],
    'oceana' => [
        'server_id' => 19,
        'key' => __DIR__ . '/storage/oceana/private.key',
        'port' => 8195,
        'levels' => 10
    ]
];